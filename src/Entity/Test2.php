<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Test2Repository")
 */
class Test2
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $blaaaa;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBlaaaa(): ?string
    {
        return $this->blaaaa;
    }

    public function setBlaaaa(string $blaaaa): self
    {
        $this->blaaaa = $blaaaa;

        return $this;
    }
}
