<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TestRepository")
 */
class Test
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $testid;

    /**
     * @ORM\Column(type="integer")
     */
    private $blabla;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTestid(): ?string
    {
        return $this->testid;
    }

    public function setTestid(string $testid): self
    {
        $this->testid = $testid;

        return $this;
    }

    public function getBlabla(): ?int
    {
        return $this->blabla;
    }

    public function setBlabla(int $blabla): self
    {
        $this->blabla = $blabla;

        return $this;
    }
}
